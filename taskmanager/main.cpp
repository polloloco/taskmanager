#include "main.hpp"

#include <cstdint>
#include <cstddef>
#include <atomic>
#include <chrono>

#include <MinHook.h>

#include <spdlog/spdlog.h>

#include "hooks.hpp"

std::atomic_bool g_has_console{ false };
std::atomic_bool g_hooked{ false };

DWORD main_thread(LPVOID reserved)
{
    using namespace std::chrono_literals;

    [[maybe_unused]] HINSTANCE dll = static_cast<HINSTANCE>(reserved);

    {
        [[maybe_unused]] const std::uintptr_t base = base_address();

        spdlog::info("taskmgr.exe at 0x{:X}", base);

        if (const bool hooked = g_hooked.exchange(true); !hooked) {
            MH_Initialize();

            spdlog::info("Enabling hooks...");

#define SETUP_HOOK(name) spdlog::info("Enabled hook {} at 0x{:X}", #name, reinterpret_cast<std::uintptr_t>(hook:: ## name ## _o)); MH_CreateHook(hook:: ## name ## _o, hook:: ## name, reinterpret_cast<LPVOID*>(&hook:: ## name ## _o))

            SETUP_HOOK(CpuHeatMap__UpdateData);

#undef SETUP_HOOK

            MH_EnableHook(MH_ALL_HOOKS);

            spdlog::info("Hooks enabled");

            auto* x = reinterpret_cast<std::uint16_t*>(base + 0xFB550 + 0x944);
            *x = g_core_count;

            spdlog::info("Spoofed");

        }

    }

    while (true) {
        if (GetAsyncKeyState(VK_F9) & 1) {
            break;
        }
        std::this_thread::sleep_for(100ms);
    }

    // this is [[noreturn]] so a return statement would be "unreachable code"
    FreeLibraryAndExitThread(dll, 0);
}

void init()
{
    if (const bool has_console = g_has_console.exchange(true); !has_console) {
        AllocConsole();
    }
}

void deinit()
{
    using namespace std::chrono_literals;

    if (const bool has_console = g_has_console.exchange(false); has_console) {
        FreeConsole();
    }

    if (const bool hooked = g_hooked.exchange(false); hooked) {
        MH_DisableHook(MH_ALL_HOOKS);
        std::this_thread::sleep_for(10ms);
        MH_Uninitialize();
    }
}
