#include "hooks.hpp"

#include <Windows.h>

#include <spdlog/spdlog.h>

#include <array>

std::uintptr_t base_address()
{
    static const std::uintptr_t base = []() -> std::uintptr_t {
        return reinterpret_cast<std::uintptr_t>(GetModuleHandle(nullptr));
    }();

    return base;
}

struct color {
    union {
        struct {
            std::uint8_t red;
            std::uint8_t green;
            std::uint8_t blue;
            std::uint8_t alpha;
        } components;
        std::uint32_t value;
    } u;
    
    color(std::uint32_t rgb)
    {
        u.components.alpha = 0xFF;
        u.components.red = rgb & 0xFF;
        u.components.green = (rgb >> 8) & 0xFF;
        u.components.blue = (rgb >> 16) & 0xFF;
    }

    color(std::uint8_t r, std::uint8_t g, std::uint8_t b, std::uint8_t a = 0xFF)
    {
        u.components.alpha = a;
        u.components.red = r;
        u.components.green = g;
        u.components.blue = b;
    }

    std::uint32_t abgr() const noexcept
    {
        return u.value;
    }
    
    std::uint8_t& r()
    {
        return u.components.red;
    }

    std::uint8_t& g()
    {
        return u.components.green;
    }

    std::uint8_t& b()
    {
        return u.components.blue;
    }

    std::uint8_t& a()
    {
        return u.components.alpha;
    }

};

std::uintptr_t hook::CpuHeatMap__UpdateData(CpuHeatMap* heatmap)
{
    static color border{ 0, 0, 0 };
    static color background{ 0, 0, 0xFF };
    static std::int32_t counter{};

    std::array<wchar_t, 10> buffer{};

    for (std::uint32_t i = 0; i < g_core_count; i++) {

        auto& r = background.r();
        auto& g = background.g();
        auto& b = background.b();

        if (counter >= 1536) {
            counter = 0;
        }

        if (counter < 256) {
            r = 0xFF;
            g = 0;
            b = 0xFF - (counter % 0x100);
        }
        else if (counter < 512) {
            r = 0xFF;
            g = (counter % 0x100);
            b = 0;
        }
        else if (counter < 768) {
            r = 0xFF - (counter % 0x100);
            g = 0xFF;
            b = 0;
        }
        else if (counter < 1024) {
            r = 0;
            g = 0xFF;
            b = (counter % 0x100);
        }
        else if (counter < 1280) {
            r = 0;
            g = 0xFF - (counter % 0x100);
            b = 0xFF;
        }
        else if (counter < 1536) {
            r = (counter % 0x100);
            g = 0;
            b = 0xFF;
        }
        counter++;

        swprintf_s(buffer.data(), buffer.size(), L"%d", i);
        hook::CpuHeatMap__SetBlockData(heatmap, i, buffer.data(), background.abgr(), border.abgr());
    }

    {
        DirectUI::Value* val;
        const auto extent = heatmap->grid->GetExtent(&val);
        spdlog::info("x: {} y: {}", extent->cx, extent->cy);
    }

    const auto descendent = heatmap->grid->FindDescendent(StrToID(L"cpuBlockData"));
    if (descendent)
    {
        DirectUI::Value* val;
        const auto extent = descendent->GetExtent(&val);
        spdlog::info("descendent x: {} y: {}", extent->cx, extent->cy);
    }
    else {
        spdlog::info("descendent was nullptr");
    }

    return 0;
}