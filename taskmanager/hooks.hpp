#pragma once

#include <cstdint>
#include <cstddef>

#include <Windows.h>

#include "sdk.hpp"

constexpr std::uint16_t g_core_count{ 1536 - (32 * 3) };

namespace offset {

	constexpr std::uint64_t CpuHeatMap__UpdateData{ 0xAB738 };

	constexpr std::uint64_t CpuHeatMap__SetBlockData{ 0xAB614 };
	constexpr std::uint64_t CpuHeatMap__GetBlockColors{ 0xAACBC };

}

std::uintptr_t base_address();

namespace hook {

#define DECL_HOOK(name, returnvalue, params) returnvalue name ## params; inline decltype(&name) name ## _o{ reinterpret_cast<decltype(&name)>(base_address() + offset:: ## name) }

	DECL_HOOK(CpuHeatMap__UpdateData, std::uintptr_t, (CpuHeatMap* heatmap));

#undef DECL_HOOK

#define DECL_FUNC(name, returnvalue, params) using name ## _t = returnvalue ## (*) ## params; inline name ## _t name{ reinterpret_cast<name ## _t>(base_address() + offset:: ##name) }

	DECL_FUNC(CpuHeatMap__SetBlockData, std::int32_t, (CpuHeatMap* heatmap, std::uint32_t element, const wchar_t*, std::uint32_t background, std::uint32_t border));
	DECL_FUNC(CpuHeatMap__GetBlockColors, void, (CpuHeatMap* heatmap, std::uint32_t element, std::uint32_t* background, std::uint32_t* border));

#undef DECL_FUNC

}