#pragma once

#include <cstdint>

#include <Windows.h>

extern "C" std::uint16_t StrToID(const wchar_t* str);

namespace DirectUI {

	class Value
	{
		char pad_0000[0x10];
	};

	class Element
	{
		virtual void vector_deleting();
	public:
		std::int32_t GetHeight();
		std::int32_t GetWidth();
		const SIZE* GetExtent(DirectUI::Value** value);
		Element* FindDescendent(std::uint16_t id);
	};


}

class CpuHeatMap
{
	virtual void Function0();

public:
	DirectUI::Element* grid;
};