#include "dllmain.hpp"
#include "main.hpp"

BOOL APIENTRY DllMain(HINSTANCE dll, DWORD reason, [[maybe_unused]] LPVOID reserved)
{
    if (reason == DLL_PROCESS_ATTACH) {
        init();
        if (const auto handle = CreateThread(nullptr, 0, main_thread, dll, 0, nullptr); handle) {
            CloseHandle(handle);
        }
        else {
            return FALSE;
        }
    }
    else if (reason == DLL_PROCESS_DETACH) {
        deinit();
    }

    return TRUE;
}
