# Taskmanager stuff

Render some basic stuff on taskmanager

![Example output](https://i.imgur.com/KB20jzS.png)

## Compiling

Compile the project using Visual Studio 2019

You can use the package manager [vcpkg](https://github.com/Microsoft/vcpkg) to install the dependencies if you don't feel like manually compiling them.

```bash
vcpkg install spdlog minhook --triplet x64-windows-static
```

## What to do with this?

Using this as a starting point you can work on displaying images or even animations in the CPU view in taskmanager